/******************************************************************************
 * File:         display.c
 * Version:      1.4
 * Date:         2018-02-20
 * Author:       M. van der Sluys, J.H.L. Onokiewicz, R.B.A. Elsinghorst, J.G. Rouland
 * Description:  OPS exercise 3: syntax check
 ******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include "displayFunctions.h"
#include <string.h>
#include <sys/resource.h>

int main(int argc, char *argv[])
{
  int ichild, numberOfCharacters;
  int niceIncr;
  char *numOfTimes = argv[2];
  char *printMethod = argv[1] ;
  char *printChar = argv[3];
  char pathname[]="../opdracht-2/template_ex02/display";
  ErrCode err;
  err = SyntaxCheck(argc, argv); // Check the command-line parameters
  if (err != NO_ERR)
  {
    DisplayError(err); // Print an error message
  }
  else
  {
    //printMethod = argv[1][0];
    //numOfTimes = strtoul(argv[2], NULL, 10); // String to unsigned long
    niceIncr = strtoul(argv[3], NULL, 10);
    numberOfCharacters = argc - 4;

        for(int i=0;i<numberOfCharacters;i++) 
   {
        if(fork() == 0)
        {
            ichild = i;
            nice(ichild*niceIncr);
            printChar = argv[4+i];
            printf("%d %d %s \n",ichild, getpriority(PRIO_PROCESS, getpid()), argv[4+i]);
	    // PrintCharacters(printMethod, numOfTimes, niceIncr, printChar); // Print character printChar numOfTimes times using method printMethod
	    if(execl(pathname, "display", printMethod, numOfTimes, printChar, (char*)NULL))
	      {
		perror("ERROR detected");
	      }
            exit(0);
        }
   }
    for(int i=0;i<numberOfCharacters;i++)
    { 
      wait(NULL);
    }
  }

  printf("\n"); // Newline at end
  return 0;
}
