/**************************************************************************
 * File:         getsignal.c
 * Version:      1.0
 * Date:         2021-05-20
 * Author        D.Punte
 * Description:  OPS exercise 5: Signals for synchronisation
 **************************************************************************/
#define _POSIX_C_SOURCE 199309L     // sigaction, struct sigaction
#define _XOPEN_SOURCE 500           // For S_IFIFO in C99+
#include <stdio.h>
#include <unistd.h>                 // write(), close(), unlink()
#include <string.h>                 // memset
#include <signal.h>                 // sigaction
#include <stdlib.h>                 // exit()
#include <sys/stat.h>               // mkfifo()
#include <fcntl.h>                  // open(), 0_* constants

void newHandler(int sig);
volatile sig_atomic_t signalCount = 0;

int main(void) {
  int fd;
  long MyPid = getpid();

  mkfifo("PIDpipe", S_IFIFO|0666); // Create FIFO; permissions: u/g/o r/w 
  fd = open("PIDpipe", O_WRONLY);  // Open FIFO write-only
  write(fd, &MyPid, sizeof(MyPid)); // Write message to FIFO
  
  struct sigaction act;
  printf("\nPID = %ld\n", MyPid);
  
  // Define SHR
  memset(&act, '\0', sizeof(act));    // Fill act with NULLs by default
  act.sa_handler = newHandler;        // Set the cutstom SHR
  act.sa_flags = 0;                   // No flags, used with act.sa_handler
  sigemptyset(&act.sa_mask);          // No signal masking during SHR execution

  // Install SHR
  sigaction(SIGXFSZ, &act, NULL);   // This cannot be SIGKILL ot SIGSTOP
  
  while(1) {
    if(signalCount > 0) {
      break;
    }else {
      write(0, "0", 1);
    }
      
    sleep(1);
    }

  int count = 0;
  while(1) {
    count = signalCount + 48;
    write(0, &count ,sizeof(count));
    sleep(1);
  }

  close(fd);
  unlink("PIDpipe");
  return 0;
}

void newHandler(int sig) {
  if(sig == SIGXFSZ) {
    signalCount++;
    if(signalCount>9){
      signalCount = 0;
    }
  }
  //exit(0);
}
