#include <stdio.h>  // printf()
#include <stdlib.h> // atoi()
#include <signal.h> // kill()
#include <unistd.h> // read(), close()
#include <fcntl.h>  // open(), O_* constants



int main(int argc, char* argv[]) {
  int fd;
  int MyPid;

  fd = open("PIDpipe", O_RDONLY);
  read(fd, &MyPid, sizeof(MyPid));
  close(fd);
  
  printf("\nPID : %d\n", MyPid);
  pid_t pid;
  pid = MyPid;
  printf("Converted PID : %d\n", pid);
  while(1){
    kill(pid, 25);
    sleep(3);
  }
  return 0;
}
