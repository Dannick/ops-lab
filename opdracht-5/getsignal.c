#define _POSIX_C_SOURCE 199309L
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>

char count = '0';
void newHandler(int sig, siginfo_t *info, void *ucontext);

int main() {

struct sigaction act, oldact;

  memset(&act, '\0', sizeof(act));  
  act.sa_sigaction = newHandler;      
  act.sa_flags = SA_SIGINFO;                 
  sigemptyset(&act.sa_mask); 
  sigaction(SIGXFSZ, &act, &oldact);
  printf("PID = %i\n",getpid());

  while (1)
  { 
   if(count > '9')
   {
      count = '0';
   }
   write(1,&count,1);
   sleep(1);
  }
  sigaction(SIGXFSZ, &oldact, NULL);

   return 0;
}

void newHandler(int sig, siginfo_t *info, void *ucontext) 
{
 count++;
}

